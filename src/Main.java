/*
 * Professor said to only worry about letters.
 * I only used .png files.
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

import javax.imageio.ImageIO;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application{
	
	Stage window;
	Scene scene;
	Button button1;
	Button button2;
	TextArea userInput;
	Image image;
	ImageView iv;
	BufferedImage img = null;
	
	public static void main(String[] args) {
		launch(args);
	}//main
	
	@Override
	public void start(Stage stage) throws Exception {
		
		window = stage;
		window.setTitle("Assignment 4");
		
		//load pic		
		try {
			img = ImageIO.read(new File("src/pic.png"));
			image = SwingFXUtils.toFXImage(img, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//imageview
		iv = new ImageView();
		iv.setImage(image);
		iv.setFitWidth(200);
		iv.setFitHeight(200);
		iv.setPreserveRatio(true);
		iv.setSmooth(true);
		
		
		//form
		VBox form = new VBox(5);
		userInput = new TextArea();
		userInput.setPrefHeight(100);
		userInput.setWrapText(true);
		
		button1 = new Button("Embed");
		button2 = new Button("Extract");
		button1.setOnAction(e-> embed() );
		button2.setOnAction(e-> extract() );
		
		form.getChildren().addAll(button1, button2);
		form.setAlignment(Pos.CENTER);
		
		//layout
		VBox layout = new VBox(10);
		layout.setPadding(new Insets(20, 20, 20, 20));
		layout.getChildren().addAll(iv, userInput, form);
		
		scene = new Scene(layout, 300, 450);
		window.setScene(scene);
		window.show();
	}//start
	
	/*
	 * Embed secret text into image.
	 */
	public void embed()
	{
		String message = userInput.getText().trim() + "\0";
		
		int width = img.getWidth();
		int height = img.getHeight();
		
		int x = 0;
		int y = 0;
		
		//loop for each char in the message
		for(int i = 0; i < message.length(); i++)
		{
			//convert char to binary
			int k = 0;
			char ch = message.charAt(i);
			String charBin = null;
			if(ch == '\0')
			{
				charBin = "0000000";
			}
			else if(ch == ' ')
			{
				charBin = "0100000";
			}
			else
			{
				charBin = Integer.toBinaryString(ch);
				
			}
			char[] charBinArray = charBin.toCharArray();
			
			//get image pixel value
			for(x = x; x < width; x++)
			{
				for(y = y; y < height; y++)
				{
					//get rgb values for pixel
					//from https://stackoverflow.com/questions/4801366/convert-rgb-values-to-integer
					int pix = img.getRGB(x, y);
					int alpha = ((pix >> 24) & 0xff);
					int red = ((pix >> 16) & 0xff);
					int green = ((pix >> 8) & 0xff);
					int blue = (pix & 0xff);
					
					//start with the r value:
					//1)convert to binary
					String redBin = Integer.toBinaryString(red);
					
					//2)then change the final bit to 0 or 1
					//depending on the binary code of the character want to add
					//to change last value
					char[] redChars = redBin.toCharArray();
					redChars[redBin.length()-1] = charBinArray[k];
					redBin = String.valueOf(redChars);
					
					//binary to int
					red = Integer.parseInt(redBin, 2);
					
					k++;
					
					//go to next letter
					if(k > charBinArray.length-1)
					{
						pix = (alpha << 24) | (red << 16) | (green << 8) | blue;
						img.setRGB(x, y, pix);
						y++;
						break;
					}
					
					//do same for g and b
					String greenBin = Integer.toBinaryString(green);
					char[] greenChars = greenBin.toCharArray();
					greenChars[greenBin.length()-1] = charBinArray[k];
					greenBin = String.valueOf(greenChars);
					
					//binary to int
					green = Integer.parseInt(greenBin, 2);
					
					k++;
					
					//go to next letter
					if(k > charBinArray.length-1)
					{
						pix = (alpha << 24) | (red << 16) | (green << 8) | blue;
						img.setRGB(x, y, pix);
						y++;
						break;
					}
					
					String blueBin = Integer.toBinaryString(blue);
					char[] blueChars = blueBin.toCharArray();
					blueChars[blueBin.length()-1] = charBinArray[k];
					blueBin = String.valueOf(blueChars);
					
					//binary to int
					blue = Integer.parseInt(blueBin, 2);
					
					k++;
					
					//go to next letter
					if(k > charBinArray.length-1)
					{
						pix = (alpha << 24) | (red << 16) | (green << 8) | blue;
						img.setRGB(x, y, pix);
						y++;
						break;
					}
					
					//set pixel to new values
					pix = (alpha << 24) | (red << 16) | (green << 8) | blue;
					img.setRGB(x, y, pix);
					
				}
				
				//go to next letter
				if(k > charBinArray.length-1)
				{
					break;
				}
				
			}//for
			
		}//for
		
		//save bufferedimage as the pic
		File output = new File("src/new.png");
		try {
			ImageIO.write(img, "png", output);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		userInput.setText("Message was embedded!");
		
	}//embed
	
	/*
	 * Extract and display secret text from image.
	 */
	public void extract()
	{
		Vector<Integer> binVec = new Vector<>();
		String message = "";
		
		//load pic
		BufferedImage img = null;
		
		try {
			img = ImageIO.read(new File("src/new.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int width = img.getWidth();
		int height = img.getHeight();
		
		int k = 0;
		
		//get image pixel value
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				//System.out.println(x + "," + y);
				
				//get rgb values for pixel
				int pix = img.getRGB(x, y);
				int red = ((pix >> 16) & 0xff);
				int green = ((pix >> 8) & 0xff);
				int blue = (pix & 0xff);
				
				//get last bit of r g b
				//red
				String redBin = Integer.toBinaryString(red);
				String redLetter = Character.toString(redBin.charAt(redBin.length()-1));
				int redBit = Integer.parseInt(redLetter);
				
				//store into vector
				binVec.add(redBit);
				
				k++;
				if(k > 6)
				{
					k=0;
					//y++;
					continue;
				}
				
				//green
				String greenBin = Integer.toBinaryString(green);
				String greenLetter = Character.toString(greenBin.charAt(greenBin.length()-1));
				int greenBit = Integer.parseInt(greenLetter);
				
				//store into vector
				binVec.add(greenBit);
				
				k++;
				if(k > 6)
				{
					k=0;
					continue;
				}
				
				//blue
				String blueBin = Integer.toBinaryString(blue);
				String blueLetter = Character.toString(blueBin.charAt(blueBin.length()-1));
				int blueBit = Integer.parseInt(blueLetter);
				
				//store into vector
				binVec.add(blueBit);
				
				k++;
				if(k > 6)
				{
					k=0;
					continue;
				}
				
			}
			
		}
		
		
		int count = 1;
		
		StringBuilder build = new StringBuilder();
		
		for(int i = 0; i < binVec.size(); i++)
		{
			
			build.append(binVec.get(i));
			
			if((count % 7) == 0)
			{
				char con = (char) Integer.parseInt(build.toString(), 2);
				
				message += con;
				
				if(con == '\0')
				{
					break;
				}
				
				//reset string builder
				build.setLength(0);
			}
			
			count++;
		}
		
		userInput.setText(message);
		
	}//extract
	
}//Main
